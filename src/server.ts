import "reflect-metadata";
import { createExpressServer } from 'routing-controllers';
import { hotelsController } from './HotelsComponent/HotelsController';
import * as bodyParser from 'body-parser';

class Server{
    public static serverInstance:Server;
    private server : any;

    public static bootstrap(): Server{
        if(!this.serverInstance){
            this.serverInstance = new Server();
        }
        return this.serverInstance;
    }

    private async createServer(){
        const app = createExpressServer({
            controllers:[
                hotelsController
            ]
        });

        app.use(bodyParser.json());

        const port = process.env.PORT || 3000;
        this.server = app.listen(port, () => {
            console.log(`App listening on the port ` + port);
        });
    }

    public constructor(){
        this.createServer();
    }
}

export const server = Server.bootstrap();