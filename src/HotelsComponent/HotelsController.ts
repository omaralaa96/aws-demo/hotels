import { Request, Response } from 'express'; 
import { JsonController, Get, Req, Res } from 'routing-controllers';
import { HotelsService } from './HotelsService';
require('dotenv').config();

@JsonController("/hotels")
export class hotelsController {

    readonly service = new HotelsService();

    @Get("")
    async getAllhotels(@Req() request: Request, @Res() response: Response){
        const res = this.service.getAllHotels();
        response.send(res);
        return response;
    } 
}